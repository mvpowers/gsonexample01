package example;

import java.io.IOException;
import java.util.Scanner;

import com.google.gson.Gson;

import json.UserPojo;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Main {
	
	private static OkHttpClient client = new OkHttpClient();
	
    public static void main (String[] args){
   	
    	Integer characterIdentifier = userInput();
    	inputCheck(characterIdentifier);
    	
    	for(String str : getUserData(characterIdentifier)){
    		System.out.println(str);
    		
    	}
    }
    
    public static Integer userInput() {
    	
    	Scanner user_input = new Scanner( System.in );
    	Integer characterId;
    	System.out.print("Enter character ID: ");
    	characterId = user_input.nextInt();
    	return characterId;
    	
    }

    public static Integer inputCheck(Integer x) {
    	
    	if((x < 86) && (x > 0)){
    	return x;
    	}
    	else{
    		System.out.println("Please enter a value 1-85");
    		return null;
    	}
    }    
    
    public static String getJson(String url) throws IOException {
    	
	  Request request = new Request.Builder()
		      .url(url)
		      .build();
	
		  Response response = client.newCall(request).execute();
		  return response.body().string();
		}
    
    
    public static String[] getUserData(Integer x){
    	
    	String json = null;
    	try {
    		json = getJson("http://swapi.co/api/people/" + x);
    	}  	
    	catch (Exception e){
    		e.printStackTrace();
    	}
    	
    	Gson gson = new Gson();
    	
    	UserPojo userPojo = gson.fromJson(json, UserPojo.class);
    	
    	return new String[]{
    		"Name: " + userPojo.getName(),
    		"Birth year: " + userPojo.getBirth_year(),
    		"Mass: " + userPojo.getMass()
    	};
    }
}